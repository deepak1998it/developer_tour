class Node:
    def __init__(self,data):
        self.data = data
        self.next = None

class LinkList:
    
    def __init__(self):
        self.head = None

    def disp(self):
        node = self.head
        while node:
            print(node.data)
            node = node.next

link = LinkList()
link.head = Node('Mon')
e2 = Node('Tue')
e3 = Node('Wed')
link.head.next = e2
e2.next = e3
link.disp()
